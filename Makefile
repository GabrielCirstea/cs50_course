# still learning about make
CC=gcc
SRC := src
LIB := lib
OBJ := obj
DIRS=$(OBJ)/week1/ $(OBJ)/week2/ $(OBJ)/week3/ $(OBJ)/week4/ $(OBJ)/week5/
EXE=test mario1 mario2 coins credit scrabble caesar substitution plurality runoff\
	tideman volume inheritance readability

SOURCES := $(wildcard $(SRC)/*.c, $(SRC)/week*/*.c)
OBJECTS := $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SOURCES))

#all: $(EXE)
#	$(CC) -I$(SRC) $< -o $(OBJ)/$@

all: $(DIRS) $(OBJECTS) $(EXE)

test: $(OBJ)/test.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

# week 1
mario1: $(OBJ)/week1/mario1.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

mario2: $(OBJ)/week1/mario2.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@


coins: $(OBJ)/week1/coins.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@ -lm

credit: $(OBJ)/week1/credit.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@ -lm

# week 2
scrabble: $(OBJ)/week2/scrabble.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

caesar: $(OBJ)/week2/caesar.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

substitution: $(OBJ)/week2/substitution.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

readability: $(OBJ)/week2/readability.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

# week 3
plurality: $(OBJ)/week3/plurality.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

runoff: $(OBJ)/week3/runoff.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

tideman: $(OBJ)/week3/tideman.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

# week 4
volume: $(OBJ)/week4/volume.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

# week 5
inheritance: $(OBJ)/week5/inheritance.o $(OBJ)/cs50.o
	$(CC) -I$(OBJ) $^ -o $@

# dirs
$(OBJ)/%/:
	mkdir $@

# objects
$(OBJ)/%.o: $(SRC)/%.c
	$(CC) -I$(SRC) $< -c -o $@

.PHONY: clean

clean:
	rm -f $(OBJ)/*.o $(EXE) $(OBJ)/week*/*.o
	rmdir $(DIRS)
