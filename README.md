# CS50 course

This is a repo with my effort to follow a CS50 course:
**CS50's Introduction to Computer Science**

## Files

In src are the folders for every week.

Some files are compiled in with **make** here, from the respective folders,
to include cs50 library. Other problems/files are compiled manually in the
corresponding folder. Ex: src/week4/recover/

### src

The files here are my implementation of cs50 library.

I also check their [github repo](https://github.com/cs50/libcs50).

The big difference is that my implementation doesn't keep track of all allocated
memory, and doesn't have a cleanup procedure.

In the other sub-directories are the problems for each week
