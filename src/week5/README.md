# Week 5

## inheritance

Generate a random blood inheritance tree.

It generates parents with random blood allele.
A child is inheriting an allele from each parent.

## speller

A simple spellchecker, that will look over a text file and compare the words from
the text with the words from the dictionary

```
Usage ./speller [DICTIONARY] text_file
```

It will perform the check on only words that contain letters and apostrophes.
This mean that words that contains numbers or other characters are ignored.
