#!/bin/python3

from cs50 import get_int

def main():
    while True:
        height = get_int("Height: ")

        if height in range(1,9):
            break

    for h in range(height):
        print(" " * (height - 1 - h), end="")
        print("#" * (h + 1), end="")
        print("  ", end="")
        print("#" *(h+1))

main()
