#!/bin/python3

from cs50 import get_string

def main():
    text = get_string("Text: ")
    words = 0
    letters = 0
    sentences = 0
    for c in text:
        if c.isalnum():
            letters += 1
        elif c.isspace():
            words += 1
        elif c in [".", "!", "?"]:
            sentences += 1
    words += 1

    print(f"Words: {words}\nletters: {letters}\nsentences: {sentences}")

    L = letters * (100.0/words);
    S = sentences * (100.0/words);
    grade = 0.0588 * L - 0.296 * S - 15.8;
    if grade < 1:
        print("Before grade 1")
    elif grade > 16:
        print("Grade 16+")
    else:
        print(f"Grade {round(grade)}")

if __name__ == "__main__":
    main()
