#!/bin/python3
import sys
import csv

def main():
    if len(sys.argv) < 3:
        print(f"Usage {sys.argv[0]} database sequence")
        sys.exit(1)
    database = open(sys.argv[1])
    reader = csv.DictReader(database)

    data = []
    for row in reader:
        data.append(row)
    database.close()

    for row in data:
        sequences = [k for k in row.keys()][1:]
        break


    f = open(sys.argv[2])
    seq = f.read()
    f.close()

    result = {}
    for s in sequences:
        n = count_consecutive(seq, s)
        result[s] = n

    for row in data:
        ok = True
        for s in sequences:
            if int(row[s]) != result[s]:
                ok = False
        if ok:
            print(row["name"])
            return
    print("No match")
    return

def count_consecutive(txt, sequence):
    # primeste sirul in care cautam secventa ADN
    # returneaza numarul maxim de apariti consecutive
    count = 0
    max_count = 0
    start = 0
    while True:
        if start > len(txt):
            break
        i = txt.find(sequence,start)
        if i < 0:
            if count > max_count:
                max_count = count
            return max_count
        if i != start:
            if count > max_count:
                max_count = count
            count = 0
        count += 1
        start = i + len(sequence)
    if count > max_count:
        max_count = count
    return max_count

if __name__ == "__main__":
    main()
