/* A simple chiper that works by shifting letters on alphabet
 * The key is provided in the comand line arguments
 * If the key is n, all letters will be shifted by n positions: the letter A will
 * be A + n. */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "cs50.h"

char upper_chip(char c, int key);
char lower_chip(char c, int key);

int main(int argc, char *argv[])
{
	if(argc != 2){
		printf("Usage: %s key\n", argv[0]);
		return 1;
	}

	// check if key si only digits
	int i;
	for(i = 0; argv[1][i]; i++)
		if(!isdigit(argv[1][i])){
			printf("Usage: %s key\n", argv[0]);
			return 1;
		}
	int key = atoi(argv[1]);
	// it's usless to have key > 26
	key %= 26;

	string plain = get_string("plaintest:  ");

	for(i = 0; plain[i]; i++){
		if(isupper(plain[i])){
			plain[i] = upper_chip(plain[i], key);
		} else if(islower(plain[i])){
			plain[i] = lower_chip(plain[i], key);
		}
	}

	printf("ciphertext: %s\n", plain);
	return 0;
}

char lower_chip(char c, int key)
{
	c -= 'a';
	c = (c + key) % 26;
	c += 'a';

	return c;
}

char upper_chip(char c, int key)
{
	c -= 'A';
	c = (c + key) % 26;
	c += 'A';

	return c;
}
