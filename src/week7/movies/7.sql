select title, rating from movies
	join ratings on ratings.movie_id = movies.id
	where year = 2010 and rating not null
	order by rating desc, title;
