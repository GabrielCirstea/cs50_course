-- Keep a log of any SQL queries you execute as you solve the mystery.

select description from crime_scene_reports
	where day=28 and month=7 and year=2020;

select name, transcript from interviews
	where day=28 and month = 7 and year = 2020;

select activity, license_plate from
	courthouse_security_logs where day=28 and
	month=7 and year=2020 and hour=10;

select name from people 
	where license_plate in
	(select license_plate from courthouse_security_logs
		where day=28 and month=7 and year=2020 and hour=10
	);


select name, transaction_type, amount from atm_transactions as t
	join bank_accounts as b on b.account_number = t.account_number
	join people as p on p.id = b.person_id
	where lower(atm_location)
	like "%fifer street%" and day=28 and month=7 and year = 2020;

-- nume de transactii in ziua respectiva
select distinct name from people
	join bank_accounts as b on b.person_id = people.id
	join atm_transactions as t on t.account_number = b.account_number
	where t.day=28 and t.month=7 and t.year=2020;

-- tranzatii si masini in security_logs
select name from people 
	where license_plate in
	(select license_plate from courthouse_security_logs
		where day=28 and month=7 and year=2020 and hour=10
	) and name in (
	select distinct name from people
		join bank_accounts as b on b.person_id = people.id
		join atm_transactions as t on t.account_number = b.account_number
		where t.day=28 and t.month=7 and t.year=2020
	);

-- phone cals
select caller, receiver from phone_calls
	where year=2020 and month=7 and day=28 and duration <= 60
	order by duration;

-- name by car and phone calls
select name from people 
	where license_plate in
	(select license_plate from courthouse_security_logs
		where day=28 and month=7 and year=2020 and hour=10
	) and phone_number in (
	select caller from phone_calls
		where year=2020 and month=7 and day=28 and duration <= 60
	);

-- flights
select count(*) from flights
	where year=2020 and month=7 and day=29;

select o.full_name, d.full_name from flights
	join airports as o on flights.origin_airport_id=o.id
	join airports as d on flights.destination_airport_id=d.id
	where lower(o.full_name) like "fiftyville regional airport"
	and day=28 and month=7 and year=2020;

-- people in flights
select p.name from passengers
	join people as p on p.passport_number = passengers.passport_number
	join flights as f on passengers.flight_id = f.id
	where f.day=29 and f.month=7 and f.year=2020;


-- license_plate + phone_call + flight + transation
select name from people 
	where license_plate in (
	select license_plate from courthouse_security_logs
		where day=28 and month=7 and year=2020 and hour=10
	) and phone_number in (
	select caller from phone_calls
		where year=2020 and month=7 and day=28 and duration <= 60
		order by duration
	) and passport_number in (
	select p.passport_number from passengers
		join people as p on p.passport_number = passengers.passport_number
		join flights as f on passengers.flight_id = f.id
		where f.id in (
		select flights.id from flights
			join airports as o on flights.origin_airport_id=o.id
			join airports as d on flights.destination_airport_id=d.id
			where lower(o.full_name) like "fiftyville regional airport"
			and day=29 and month=7 and year=2020
		)
	) and name in (
	select name from atm_transactions as t
		join bank_accounts as b on b.account_number = t.account_number
		join people as p on p.id = b.person_id
		where lower(atm_location)
		like "%fifer street%" and day=28 and month=7 and year = 2020
		and transaction_type like "withdraw"
	);

-- licence_plate + phone_call + flight
select name from people 
	where license_plate in
	(select license_plate from courthouse_security_logs
		where day=28 and month=7 and year=2020 and hour=10
	) and phone_number in (
	select caller from phone_calls
		where year=2020 and month=7 and day=28 and duration <= 60
		order by duration
	) and passport_number in (
	select p.passport_number from passengers
		join people as p on p.passport_number = passengers.passport_number
		join flights as f on passengers.flight_id = f.id
		where f.day=29 and f.month=7 and f.year=2020
	);

-- complice din phone_call + flights
select name from people
	where phone_number in (
	select receiver from phone_calls
		where year=2020 and month=7 and day=28 and duration <= 60
		order by duration
	) and passport_number in (
	select p.passport_number from passengers
		join people as p on p.passport_number = passengers.passport_number
		join flights as f on passengers.flight_id = f.id
		where f.id in (
		select flights.id from flights
			join airports as o on flights.origin_airport_id=o.id
			join airports as d on flights.destination_airport_id=d.id
			where lower(o.full_name) like "fiftyville regional airport"
			and day=28 and month=7 and year=2020
		)
	);

-- taote persoanele care pleaca din oras
select flights.id, d.full_name, people.name from flights
	join airports as o on flights.origin_airport_id=o.id
	join airports as d on flights.destination_airport_id=d.id
	join passengers as p on p.flight_id = flights.id
	join people on people.passport_number = p.passport_number
	where lower(o.full_name) like "fiftyville regional airport"
	and day=29 and month=7 and year=2020;
