/* cs50.c
 * My implementation of cs50 library
 * Official implementation here: https://github.com/cs50/libcs50*/
#include <limits.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>

#include "cs50.h"

#define CAP_INCREASE 8

static void* increase_cap(void* buffer, size_t size, size_t* cap)
{
	int capacity = *cap;
	if(size + 1 > capacity){
		// check for space
		if(capacity + CAP_INCREASE < SIZE_MAX){
			capacity += CAP_INCREASE;
		} else {
			free(buffer);
			return NULL;
		}

		void* tmp = realloc(buffer, capacity);
		if(!tmp){
			free(buffer);
			return NULL;
		}
		buffer = tmp;
	}
	*cap = capacity;
	return buffer;
}

string get_string(const char* format, ...)
{
	string buffer = NULL;

	size_t capacity = 0;
	size_t size = 0;

	if(format){
		/* print the requested string */
		va_list va;
		va_start(va, format);
		vprintf(format, va);
		va_end(va);
	}

	char c;
	while((c = fgetc(stdin)) != '\r' && c != '\n' && c != EOF){
		buffer = increase_cap(buffer, size, &capacity);
		if(!buffer)
			return NULL;

		buffer[size++] = c;
	}

	// too much input, no room for \0
	if(size == SIZE_MAX){
		free(buffer);
		return NULL;
	}

	// put \0
	buffer = increase_cap(buffer, size, &capacity);
	if(!buffer)
		return NULL;
	buffer[size++] = '\0';

	return buffer;
}

char get_char(const char* format, ...)
{
	va_list va;
	va_start(va, format);

	while(1){
		vprintf(format, va);
		string line = get_string(NULL);
		if(!line){
			va_end(va);
			return CHAR_MAX;
		}

		char c, d;
		if(sscanf(line, "%c%c", &c, &d) == 1){
			va_end(va);
			return c;
		}
	}
}

int get_int(const char* format, ...)
{
	va_list va;
	va_start(va, format);
	
	while(1){
		vprintf(format, va);
		string line = get_string(NULL);
		if(!line){
			va_end(va);
			return INT_MAX;
		}
		if(strlen(line) > 0 && !isspace((unsigned char)line[0])){
			char *tail;
			errno = 0;
			long n = strtol(line, &tail, 10);
			if(errno == 0 && *tail == '\0' && n > INT_MIN && n < INT_MAX){
				va_end(va);
				return n;
			}
		}
	}
}

long get_long(const char* format, ...)
{
	va_list va;
	va_start(va, format);
	
	while(1){
		vprintf(format, va);
		string line = get_string(NULL);
		if(!line){
			va_end(va);
			return LONG_MAX;
		}
		if(strlen(line) > 0 && !isspace((unsigned char)line[0])){
			char *tail;
			errno = 0;
			long n = strtol(line, &tail, 10);
			if(errno == 0 && *tail == '\0' && n < LONG_MAX){
				va_end(va);
				return n;
			}
		}
	}
}

long long get_long_long(const char* format, ...)
{
	va_list va;
	va_start(va, format);
	
	while(1){
		vprintf(format, va);
		string line = get_string(NULL);
		if(!line){
			va_end(va);
			return LLONG_MAX;
		}
		if(strlen(line) > 0 && !isspace((unsigned char)line[0])){
			char *tail;
			errno = 0;
			long long n = strtoll(line, &tail, 10);
			if(errno == 0 && *tail == '\0' && n < LLONG_MAX){
				va_end(va);
				return n;
			}
		}
	}
}

float get_float(const char* format, ...)
{
	va_list va;
	va_start(va, format);
	
	while(1){
		vprintf(format, va);
		string line = get_string(NULL);
		if(!line){
			va_end(va);
			return FLT_MAX;
		}
		if(strlen(line) > 0 && !isspace((unsigned char)line[0])){
			char *tail;
			errno = 0;
			float n = strtof(line, &tail);
			if(errno == 0 && *tail == '\0' && isfinite(n) && n < FLT_MAX){
				// nu permite hexa, puteri
				// strcpn == strlen fara caracterele mentionate.
				if(strcspn(line, "EeXxPp") == strlen(line)){
					va_end(va);
					return n;
				}
			}
		}
	}
}

double get_double(const char* format, ...)
{
	va_list va;
	va_start(va, format);
	
	while(1){
		vprintf(format, va);
		string line = get_string(NULL);
		if(!line){
			va_end(va);
			return DBL_MAX;
		}
		if(strlen(line) > 0 && !isspace((unsigned char)line[0])){
			char *tail;
			errno = 0;
			float n = strtod(line, &tail);
			if(errno == 0 && *tail == '\0' && isfinite(n) && n < DBL_MAX){
				// nu permite hexa, puteri
				// strcpn == strlen fara caracterele mentionate.
				if(strcspn(line, "EeXxPp") == strlen(line)){
					va_end(va);
					return n;
				}
			}
		}
	}
}
