/* cs50.h
 * My implementation of cs50 library
 * Official implementation here: https://github.com/cs50/libcs50*/

#ifndef _CS50_H
#define _CS50_H

typedef char* string;

typedef char bool;
#define true 1
#define false 0

char get_char(const char* format, ...);
int get_int(const char* format, ...);
float get_float(const char* format, ...);
string get_string(const char* format, ...);
long get_long(const char* fotmat, ...);
long long get_long_long(const char* format, ...);

#endif // iddef cs50_h
