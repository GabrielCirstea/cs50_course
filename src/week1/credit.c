/* Validate and identify the credid card's number*/
#include <stdio.h>
#include "cs50.h"

int digit_sum(int n)
{
	int sum = 0;
	while(n){
		sum += n%10;
		n/=10;
	}

	return sum;
}

int main()
{
	long number;
		number = get_long("Card number: ");

	long copy = number;
	int digits = 0;
	int double_sum = 0,	// sum of digits multiplyed by 2
		sum = 0;		// sum of the other digits

	while(copy > 9){
		int second_last = copy % 100;
		sum += second_last % 10;
		second_last /= 10;
		double_sum += digit_sum(2 * second_last);
		digits += 2;
		copy /= 100;
	}

	// has an odd number of digits
	if(copy > 0){
		digits++;
		sum += copy;
	}

	int sum_check = sum + double_sum;
	if(sum_check % 10 != 0){
		printf("digits=%d\n", digits);
		printf("sum_check=%d\n", sum_check);
		printf("INVALID!\n");
		return 0;
	}

	copy = number;
	while(copy > 100){
		copy /= 10;
	}

	if(copy == 34 || copy == 37){
		printf("AMEX\n");
		return 0;
	}

	if(copy >= 51 && copy <= 55){
		printf("MASTERCARD\n");
		return 0;
	}

	if(copy/10 == 4 && (digits == 13 || digits == 16)){
		printf("VISA\n");
		return 0;
	}

	printf("INVALID\n");
	return 0;
}
