/* Find the smallest numeber of coins to cover a value
 * Cons: 25, 10, 5, 1*/
#include <stdio.h>
#include <math.h>
#include "cs50.h"

int main()
{
	// read value from stdin
	float f;
	do{
		f = get_float("Value: ");
	}while(f <= 0.0);

	// convert to pennies
	int value = round(f * 100);

	int nr_coins = 0;
	while(value >= 25){
		value -= 25;
		nr_coins++;
	}
	while(value >= 10){
		value -= 10;
		nr_coins++;
	}
	while(value >= 5){
		value -= 5;
		nr_coins++;
	}
	while(value >= 1){
		value -= 1;
		nr_coins++;
	}

	printf("%d\n", nr_coins);
}
