// call the main function
window.onload = loadedScript;

function loadedScript() {
	// this is the main functio
	imgClick();
	appendClearBoth();
}

// click on image to open it
function imgClick(){
	let container = document.getElementsByClassName("container");
	let sections = container[0].getElementsByTagName("section");
	for(section of sections){
		let images = section.getElementsByTagName("img");
		for(let i=0;i<images.length;i++){
			images[i].addEventListener("click",() => window.open(images[i].src,"_self"), true);
		}
	}
}

// to prevent images by getting in other sections append a special div at the end of
// the section
function appendClearBoth(){
	let sections = document.getElementsByTagName("section");
	for(let i=0; i<sections.length;i++){
		// nu stiu de ce nu mai vede inalt daca il fac inafara for-ului
		var inalt = document.createElement('DIV');
		inalt.className = "inaltator";
		sections[i].appendChild(inalt);
	}
}
